import { useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import logo from '../../assets/rp-logo.png'
import { RootState } from '../../redux/store'
import './SideNav.scss'

export default function SideNav() {

    const visibility = useSelector((state: RootState) => state.primary.navbarVisible)
    const navigate = useNavigate()


    return(
        <div className={visibility ? 'sidenav-wrapper-visible' : 'sidenav-wrapper'}>
            <div className='sidenav-header'>
                <img src={logo} alt="company-logo" />
            </div>
            <ul className='nav-items'>
                <li className='nav-item' onClick={() => navigate('/home')}>Dashboard</li>
                <div className='mid-border'></div>
                <li className='nav-item' onClick={() => navigate('/catalog')}>Product Catalog</li>
            </ul>
        </div>
    )
}