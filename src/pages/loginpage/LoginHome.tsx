import LoginForm from "./LoginForm"
import logo from "../../assets/rp-logo.png"
import './Login.scss'


function LoginHome() {
    return(
        <div className="login-home-wrapper">
            <img src={logo} alt="company-logo" className="rp-logo"/>
            <div className="home-title-wrapper">
                <h1>WELCOME TO RED PANDA UI HTML-CSS BASIC FED TEMPLATE</h1>
                <p>The Red Pandas is a team of pragmatic engineers who care deeply about the quality of their products and success of their partners. Over the years we have mastered the best practices in building software products from concept to scale and we continue to learn emerging technologies.</p>
            </div>
            <LoginForm />
            <p>© 2022 Red Pandas. All Rights Reserved.</p>
        </div>
    )
}

export default LoginHome