import { useEffect, useState } from "react"
import { useDispatch } from "react-redux"
import { useNavigate } from "react-router-dom"
import { ToastContainer, toast } from "react-toastify"
import 'react-toastify/dist/ReactToastify.css'
import { UserData } from "../../data"
import { addLoggedInUser } from "../../redux/slice"
import './Login.scss'


export default function LoginForm() {
    const [username, setUserName] = useState("")
    const [password, setPassword] = useState("")

    const dispatch = useDispatch()

    useEffect(() => {
        let userinfo = localStorage.getItem('rp-user')
        if (userinfo) {
            let userinfoObj = JSON.parse(userinfo)
            setUserName(userinfoObj.username)
            setPassword(userinfoObj.password)
        }
    },[])

    const navigate = useNavigate()
    const [rememberMe, setRememberMe] = useState(false)

    const handleSignin = (e: any) => {
        e.preventDefault()
        let dataObj = {
            username,
            password
        }
        let validateCheck = UserData.find(user => user.username === username && user.password === password)
        if (validateCheck) {
            toast.info('Logging In!')
            if (rememberMe) {
                localStorage.setItem('rp-user', JSON.stringify(dataObj))
            }
            dispatch(addLoggedInUser(validateCheck))
            setTimeout(() => {
                navigate('/home')
            }, 3000)
        } else{
            toast.error('Enter Valid Credentials!')
        }

    }

    const handleUsernameChange = (e: any) => {
        setUserName(e.target.value)
    }

    const handlePasswordChange = (e: any) => {
        setPassword(e.target.value)
    }

    return(
        <form className="login-form-wrapper" onSubmit={handleSignin}>
            <ToastContainer position="top-center" />
            <h2>PLEASE LOG IN</h2>
            <label htmlFor="username" className="form-label" >Username</label>
            <input autoComplete="off" onChange={handleUsernameChange} value={username} type="text" name="username" id="username" required placeholder="Email or Username" />
            <label htmlFor="password" className="form-label">Password</label>
            <input autoComplete="off" onChange={handlePasswordChange} value={password} type="password" required name="password" id="password" placeholder="Password" />
            <div>
                <input onChange={() => setRememberMe(!rememberMe)} checked={rememberMe} type="checkbox" name="remember-me" id="remember-me" />
                <label htmlFor="remember-me" style={{display: 'inline'}}>Remember me</label>
            </div>
            <button className="btn">Sign In</button>
        </form>
    )
}