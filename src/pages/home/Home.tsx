import DashboardHome from "../dashboard/DashboardHome";
import Header from "../header/Header";
import { DashboardData } from "../../data";
import SideNav from "../sidenav/SideNav";
import './Home.scss'


export default function Home() {
    
    return(
        <div className="layout-wrapper">
            <SideNav />
            <div>
                <Header />
                <DashboardHome props={DashboardData} />
            </div>
        </div>
    )
}