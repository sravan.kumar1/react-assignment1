import { progressCardType } from "../Types"
import './ProgressCard.scss'

interface ProgressPropsType{
    data: progressCardType
}


export default function ProgressCard(data: ProgressPropsType) {

    const {title, subtitle, change, color, displayNum} = data.data
    
    return(
        <div className="progress-card card">
            <div className="flex justify-content-between">
                <div className="flex flex-column">
                    <strong>{title}</strong>
                    <label htmlFor="profit-bar">{subtitle}</label>
                </div>
                <div>
                    <h3>{displayNum}</h3>
                </div>
            </div>
            <div >
                <div className="meter">
                    <span style={{width: change, backgroundColor: color}}></span>
                </div>
            </div>
            <div className="flex justify-content-between">
                <p>Change</p>
                <p>{change}</p>
            </div>
        </div>
    )
}