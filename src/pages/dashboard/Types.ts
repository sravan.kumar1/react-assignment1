export interface referralsType{
    name: string,
    position: string,
    age: number,
    office: string,
    date: string,
    salary: string
}

export interface progressCardType{   
    title: string,
    subtitle: string,
    displayNum: string,
    change: string
    color: string,
}

export interface cardsType{
    name: string,
    balance: string,
    cardNumber: string,
    cardCompany: string,
    expiry: string
}

export interface teamsType{
    name: string,
    members: string[],
    currentMonthCharged: string
}

export interface dashDataType{
    props: {
        progressInformation: progressCardType[],
        recentReferrals: referralsType[],
        cards: cardsType[],
        teams: teamsType[]
    }
}