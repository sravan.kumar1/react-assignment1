import { cardsType } from "./Types";
import './Dashboard.scss'

interface CcType{
    data: cardsType
}

export default function CreditCard(data: CcType) {
    return(
        <div className="credit-card">
            <svg className="svg-icon" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="50" height="50" viewBox="0 0 172 172" style= {{fill:"#000000"}}><g fill="none" fillRule="nonzero" stroke="none" strokeWidth="1" strokeLinecap="butt" strokeLinejoin="miter" strokeMiterlimit="10" strokeDasharray="" strokeDashoffset="0" fontFamily="none" fontWeight="none" fontSize="none" textAnchor="none" style={{mixBlendMode: "normal"}}><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#a9a9a9"><path d="M44.075,6.88c-1.63937,0.30906 -2.82187,1.76031 -2.795,3.44v151.36c-0.01344,1.23625 0.645,2.37844 1.70656,2.99656c1.06156,0.63156 2.37844,0.63156 3.45344,0.01344l39.56,-23.1125l39.56,23.1125c1.075,0.61813 2.39188,0.61813 3.45344,-0.01344c1.06156,-0.61813 1.72,-1.76031 1.70656,-2.99656v-151.36c0,-1.89469 -1.54531,-3.44 -3.44,-3.44h-82.56c-0.1075,0 -0.215,0 -0.3225,0c-0.1075,0 -0.215,0 -0.3225,0zM48.16,13.76h75.68v141.9l-36.12,-21.07c-1.06156,-0.61812 -2.37844,-0.61812 -3.44,0l-36.12,21.07z"></path></g></g></svg>
            {/* <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="50" height="50" viewBox="0 0 172 172" style={{fill:"#000000"}}><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style={{mixBlendMode: "normal"}}><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#a9a9a9"><path d="M20.64,3.44v13.76c0,5.65719 4.66281,10.32 10.32,10.32h6.88v23.1125c0,10.34688 5.14656,20.06219 13.76,25.8l14.405,9.5675l-14.405,9.5675c-8.61344,5.73781 -13.76,15.45313 -13.76,25.8v23.1125h-6.88c-5.65719,0 -10.32,4.66281 -10.32,10.32v13.76h130.72v-13.76c0,-5.65719 -4.66281,-10.32 -10.32,-10.32h-6.88v-23.1125c0,-10.34687 -5.14656,-20.06219 -13.76,-25.8l-14.405,-9.5675l14.405,-9.5675c8.61344,-5.73781 13.76,-15.45312 13.76,-25.8v-23.1125h6.88c5.65719,0 10.32,-4.66281 10.32,-10.32v-13.76zM27.52,10.32h116.96v6.88c0,1.94844 -1.49156,3.44 -3.44,3.44h-110.08c-1.94844,0 -3.44,-1.49156 -3.44,-3.44zM44.72,27.52h82.56v23.1125c0,8.04906 -4.04469,15.53375 -10.75,19.995l-18.705,12.47l-4.3,2.9025l4.3,2.9025l18.705,12.47c6.70531,4.47469 10.75,11.94594 10.75,19.995v23.1125h-82.56v-23.1125c0,-8.04906 4.04469,-15.52031 10.75,-19.995l18.705,-12.47l4.3,-2.9025l-4.3,-2.9025l-18.705,-12.47c-6.70531,-4.46125 -10.75,-11.94594 -10.75,-19.995zM30.96,151.36h110.08c1.94844,0 3.44,1.49156 3.44,3.44v6.88h-116.96v-6.88c0,-1.94844 1.49156,-3.44 3.44,-3.44z"></path></g></g></svg> */}
            <h3>{data.data.name}</h3>
            <p>{data.data.cardNumber}</p>
            <h4>{data.data.cardCompany}</h4>
            <p className="expiry-label">{data.data.expiry}</p>
        </div>
    )
}