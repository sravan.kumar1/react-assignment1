import { referralsType } from "./Types";
import './Dashboard.scss'


interface refType{
    data: referralsType[]
}


export default function ReferralMain(props: refType) {
    return(
        <div className="referral-main card">
            <p className="main-label" style={{padding: "0.5rem 0"}}>RECENTLY REFERRALS</p>
            <p className="subtitle-label">Block with important Recently Referrals information</p>
            <table className="table-wrapper">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Age</th>
                        <th>Office</th>
                        <th>Date</th>
                        <th>Salary</th>
                    </tr>
                </thead>
                <tbody>
                    {props.data.map((refer, i) => <tr key={i}>
                        <td>{refer.name}</td>
                        <td>{refer.position}</td>
                        <td>{refer.age}</td>
                        <td>{refer.office}</td>
                        <td>{refer.date}</td>
                        <td>{refer.salary}</td>
                    </tr>)}
                </tbody>
            </table>
        </div>
    )
}