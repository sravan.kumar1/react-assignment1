import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { RootState } from "../../redux/store";
import CreditCard from "./CreditCard";
import ProgressCard from "./progress_cards/ProgressCard";
import ReferralMain from "./ReferralMain";
import TeamCard from "./TeamCard";
import { dashDataType } from "./Types";
import './Dashboard.scss'


export default function DashboardHome(props: dashDataType) {
    const { progressInformation, cards, teams, recentReferrals} = props.props
    const navigate = useNavigate()
    
    const userLoggedIn = useSelector((state: RootState) => state.primary.loggedIn)
    useEffect(() => {
        if (!userLoggedIn) {
            navigate('/')
        }
    },[navigate, userLoggedIn])
    return(
        <div className="home-wrapper">
            <div>
                <p className="main-label">PROGRESS INFORMATION</p>
                <div className="progress-card-wrapper">
                    {progressInformation.map((card, i) => <ProgressCard data={card} key={i} />)}
                </div>
            </div>
            <div className="recent-refer-wrapper">
                <ReferralMain data={recentReferrals} />
            </div>
            <div className="your-cards-wrapper">
                <p className="main-label">YOUR CARDS (3)</p>
                <div className="credit-cards-container">
                    {cards.map((card, i) => <CreditCard data={card} key={i} />)}
                </div>
            </div>
            <div className="your-teams-wrapper">
                <p className="main-label">YOUR TEAMS (4)</p>
                <div className="team-cards-wrapper">
                    {teams.map((team, i) => <TeamCard data={team} key={i} />)}
                </div>
            </div>
        </div>
    )
}