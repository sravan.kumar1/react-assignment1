import { teamsType } from "./Types";
import './Dashboard.scss'

interface tType{
    data: teamsType
}

export default function TeamCard(data: tType) {
    return(
        <div className="card" style={{padding: '2rem 2rem 0 2rem'}}>
            <h3>{data.data.name}</h3>
            <div className="profile-images-wrapper">
                {data.data.members.map((member, i) => <div id="profileimage" key={i}>{member}</div>)}
            </div>
            <p className="expiry-label">
                Current Month Charged: <br />
                {data.data.currentMonthCharged}
            </p>
        </div>
    )
}