

export interface CatalogDataType{
    id: number,
    productName: string;
    currentPrice: string;
    discountedPrice: string;
    new: boolean;
    previewImage: string;
}