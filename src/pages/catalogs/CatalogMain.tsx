import { Outlet, useNavigate, useParams } from "react-router-dom";
import Header from "../header/Header";
import SideNav from "../sidenav/SideNav";
import ProductCard from "./ProductCard";
import { CatData } from "../../data";
import { useSelector } from "react-redux";
import { RootState } from "../../redux/store";
import { useEffect } from "react";
import './Catalogs.scss'


export default function CatalogMain() {
    const params = useParams()
    const navigate = useNavigate()
    
    const userLoggedIn = useSelector((state: RootState) => state.primary.loggedIn)
    useEffect(() => {
        if (!userLoggedIn) {
            navigate('/')
        }
    },[navigate, userLoggedIn])
    
    return(
        <div className="layout-wrapper">
            <SideNav />
            <div>
                <Header />
                {params.productId ? <Outlet/> : <div className="home-wrapper">
                    <div className="card">
                        <p className='main-label'>PRODUCTS CATALOG</p>
                        <div className="products-wrapper">
                            {CatData.map((product, i) => <ProductCard data={product} key={i} />)}
                        </div>
                    </div>
                </div>}
            </div>
        </div>
    )
}