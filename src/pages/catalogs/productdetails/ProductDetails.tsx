import { useNavigate, useParams } from "react-router-dom"
import { toast } from "react-toastify"
import 'react-toastify/dist/ReactToastify.css'
import backIcon from '../../../assets/back-icon.png'
import './ProductDetails.scss'
import productImage1 from '../../../assets/products/001.jpg'
import productImage2 from '../../../assets/products/002.jpg'
import productImage3 from '../../../assets/products/003.jpg'
import productImage4 from '../../../assets/products/004.jpg'
import CatalogFooter from "./CatalogFooter"
import { useState } from "react"
import SellerDetails from "./sellerdetails/SellerDetails"
import { useDispatch } from "react-redux"
import { addToCart } from "../../../redux/slice"


const sellerDetails = {
    title: 'Koko Networks',
    desciption: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus sunt, voluptas praesentium repellat architecto assumenda nesciunt porro labore dolores voluptate maiores nemo mollitia soluta similique minus tenetur explicabo eius quas! Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus sunt, voluptas praesentium Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusamus sunt, voluptas praesentium"
}

export default function ProductDetails() {
    const params = useParams()
    console.log(params.productId);

    const dispatch = useDispatch()
    
    const navigate = useNavigate()

    const [currentImage, setCurrentImage] = useState(productImage1)
    const [showSeller, setShowSeller] = useState(false)
    const [currentTab, setCurrentTab] = useState('info')

    const handleBackClick = () => {
        navigate(-1)
    }

    const handleSellerClick = () => {
        setShowSeller(!showSeller)
    }

    const handleImageClick = (e: any) => {
        setCurrentImage(e.target.src)
    }

    const handleAddCart = () => {
        if (!params.productId) return
        dispatch(addToCart(params.productId))
        toast.success('Added to Cart!')
    }

    const handleTabClick = (tab: string) => {
        setCurrentTab(tab)
    }
    
    return(
        <div className="home-wrapper">
            {/* <ToastContainer position="top-center" /> */}
            {showSeller ? <SellerDetails title={sellerDetails.title} description={sellerDetails.desciption} closeFn={handleSellerClick} /> : null}
            <div className="back-button" onClick={handleBackClick}>
                <img src={backIcon} alt="" />
                <p>Back</p>
            </div>
            <div className="card">
                <p className="main-label">PRODUCT DETAILS</p>
                <div className="product-details-wrapper">
                    <div>
                        <div className="product-details-card">
                            <div className='product-details-image'>
                                <div className='new-ribbon'>NEW</div>
                                <img width={250} src={currentImage} alt="currentPreview" />
                            </div>
                        </div>
                        <div className="preview-images">
                                <img onClick={handleImageClick} width={80} src={productImage2} alt="product-preview" />
                                <img onClick={handleImageClick} width={80} src={productImage3} alt="product-preview" />
                                <img onClick={handleImageClick} width={80} src={productImage4} alt="product-preview" />
                                <img onClick={handleImageClick} width={80} src={productImage1} alt="product-preview" />
                                <img onClick={handleImageClick} width={80} src={productImage3} alt="product-preview" />
                                <img onClick={handleImageClick} width={80} src={productImage1} alt="product-preview" />
                        </div>
                    </div>
                    <div>
                        <div className="product-tags">
                            <p>Catalog</p>
                            <p>Chair</p>
                            <p>With background</p>
                            <p>White</p>
                        </div>
                        <h2 className="product-details-name">Beautiful Chairs for Kids</h2>
                        <div className="product-details-prices">
                            <h3>$189.95</h3>
                            <p>$220.95</p>
                        </div>
                        <div className="product-sizes-description">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae commodi quo corrupti eum hic placeat officia ex error in quos et, dolorem autem excepturi quaerat veritatis voluptatibus quia doloribus perspiciatis.</p>
                            <p>Available Sizes</p>
                            <div className="flex space-between">
                                <div className='flex sizes-selection'>
                                    <p>S</p>
                                    <p>M</p>
                                    <p>XL</p>
                                </div>
                                <button onClick={handleAddCart} className="btn-cart">Add to Cart</button>
                            </div>
                        </div>
                        <p className="seller-link" onClick={handleSellerClick}>About Seller</p>
                        <div className="info-description">
                            <p onClick={()=>handleTabClick('info')} className={currentTab === 'info' ? 'active' : ''}>Information</p>
                            <p onClick={()=>handleTabClick('description')} className={currentTab === 'description' ? 'active': ''}>Description</p>
                        </div>
                        <div className={currentTab === 'info' ? "description-lists active-content": 'description-lists'}>
                            <div>
                                <strong>Description lists</strong>
                                <p>A description list is perfect for defining terms</p>
                            </div>
                            <div>
                                <strong>Description lists</strong>
                                <p>A description list is perfect for defining terms</p>
                            </div>
                            <div>
                                <strong>Description lists</strong>
                                <p>A description list is perfect for defining terms</p>
                            </div>
                            <div>
                                <strong>Description lists</strong>
                                <p>A description list is perfect for defining terms</p>
                            </div>
                        </div>
                        <div className={currentTab === 'description' ? 'description-lists active-content': 'description-lists'}>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis possimus quam cum nemo nam! Tenetur delectus perferendis alias pariatur aperiam, inventore, autem repudiandae commodi quis sint, vero nemo praesentium! Cupiditate!</p>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Iusto excepturi culpa fugit atque, blanditiis harum et, nihil aliquid quidem quasi veniam quis ipsam, magni molestias consequuntur voluptates vero id ex.</p>
                        </div>
                    </div>
                </div>
            </div>
            <CatalogFooter />
        </div>
    )
}