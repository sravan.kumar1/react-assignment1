

export default function CatalogFooter() {
    return(
        <div className="footer-wrapper">
            <strong>Red Panda UI HTML - CSS Basic Template - FED!</strong>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore, aperiam adipisci mollitia repellat vitae voluptatum. Quae labore aliquid doloremque magnam quibusdam omnis fuga, dolor ipsam reprehenderit voluptate vel mollitia iusto.</p>
        </div>
    )
}