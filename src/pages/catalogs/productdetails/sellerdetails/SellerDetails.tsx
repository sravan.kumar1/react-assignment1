import './SellerDetails.scss'
import closeIcon from '../../../../assets/close-icon.png'

interface sellerPropsType{
    title: string,
    description: string,
    closeFn: () => void
}

export default function SellerDetails(props: sellerPropsType) {
    return(
        <div className="seller-modal-wrapper">
            <div className="modal-header">
                <h1>{props.title}</h1>
                <img src={closeIcon} height={20} onClick={props.closeFn} alt='close-icon' />
            </div>
            <p className='modal-description'>{props.description}</p>
            <div className='modal-footer'>
                <button onClick={props.closeFn}>Close</button>
            </div>
        </div>
    )
}