import { useNavigate } from 'react-router-dom'
import { CatalogDataType } from './Types' 
import './Catalogs.scss'

interface CatData{
    data: CatalogDataType
}

export default function ProductCard(data: CatData) {
    const navigate = useNavigate()
    const handleProductClick = () => {
        navigate(`${data.data.id}`)
    }
    return(
        <div className='product-card' onClick={handleProductClick}>
            <div className='product-image'>
                {data.data.new ? <div className='new-ribbon'>NEW</div> : null}
                <img width={250} src={data.data.previewImage} alt="preview" />
            </div>
            <div className='product-name-wrapper'>
                <strong className='product-name'>{data.data.productName}</strong>
                <h2 className='product-price'>{data.data.currentPrice}</h2>
            </div>
            <div className='flex justify-content-between pb-1'>
                <div className='flex sizes-selection'>
                    <p>S</p>
                    <p>M</p>
                    <p>XL</p>
                </div>
                <div className='product-discount'>
                    <p>{data.data.discountedPrice}</p>
                </div>
            </div>
            <p className='italic-label'>Including Lenses</p>
        </div>
    )
}