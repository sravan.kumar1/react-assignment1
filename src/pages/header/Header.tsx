import avatar from '../../assets/user-avatar.png'
import dropdown from '../../assets/arrow-down.png'
import { useNavigate } from 'react-router-dom'
import { ToastContainer, toast } from "react-toastify"
import 'react-toastify/dist/ReactToastify.css'
import { useDispatch } from 'react-redux'
import { logoutUser, toggleNav } from '../../redux/slice'
import './Header.scss'

export default function Header() {

    const dispatch = useDispatch()
    
    const navigate = useNavigate()
    const handleLogout = () => {
        toast.warning('Logging Out!')
        setTimeout(() => {
            navigate('/')
        }, 3000)
        dispatch(logoutUser())
    }

    const handleToggleSidenav = () => {
        dispatch(toggleNav())
    }

    return(
        <div className="header-wrapper">
            <img alt='hamburger-icon' className='hamburger-icon' onClick={handleToggleSidenav} height={30} src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABmJLR0QA/wD/AP+gvaeTAAAAOElEQVRoge3WIQ4AIBADwR7//zNYPAkHyYyqrNwEAEhte7a9OFNJMrpfAADANTIeAAC+I+MBgAct9bEDCpcl7tsAAAAASUVORK5CYII="/>
                <ToastContainer position='top-center' />
            <div className='profile-wrapper'>
                <img width={40} height={40} src={avatar} alt="user-avatar" className='user-avatar' />
                <img src={dropdown} alt="dropdown" width={10} height={10} className='dropdown'/>
                <button onClick={handleLogout} className='logout-btn'>Logout</button>
            </div>
        </div>
    )
}