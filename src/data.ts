import product1 from './assets/products/001.jpg'
import product2 from './assets/products/002.jpg'
import product3 from './assets/products/003.jpg'
import product4 from './assets/products/004.jpg'

export const UserData = [
    {
        userId: 1,
        username: 'sravan',
        password: 'password123'
    },
    {
        userId: 2,
        username: 'john',
        password: 'pass321'
    },
    {
        userId: 3,
        username: 'foo',
        password: 'bar'
    }
]

export const DashboardData = {
    progressInformation: [
        {
            title: 'Total Profit',
            subtitle: 'Profit',
            displayNum:'$18M',
            change: '78%',
            color: "rgb(0, 88, 255)"
        },
        {
            title: 'Feedbacks',
            subtitle: 'Customer review',
            displayNum: "134",
            change: '89%',
            color: 'rgb(0,196,136)'
        },
        {
            title: 'New Orders',
            subtitle: 'Orders Progress',
            displayNum: "160",
            change: '100%',
            color: 'rgb(255,24,52)'
        },
        {
            title: 'New Users',
            subtitle: 'Joined Users',
            displayNum: "382",
            change: '77%',
            color: 'rgb(170,183,192)'
        }
    ],
    recentReferrals: [
        {
            name: 'Damon',
            position: '5516 Adolfo Green',
            age: 18,
            office: 'Littelhaven',
            date: '2014/06/13',
            salary: '553.536'
        },
        {
            name: 'Miracle',
            position: '176 Hirthe Squares',
            age: 35,
            office: 'Ryleetown',
            date: '2013/09/27',
            salary: '784.802'
        },
        {
            name: 'Torrey',
            position: '1995 Hirthe Squares',
            age: 15,
            office: 'West Sedrickstad',
            date: '2014/09/12',
            salary: '344.302'
        }
    ],
    cards: [
        {
            name: 'David Bekham',
            balance: '$560,245.35',
            cardNumber: '8748-XXXX-1678-5416',
            cardCompany: 'MASTERCARD',
            expiry: 'Expires at 03/22'
        },
        {
            name: 'Matt Daemon',
            balance: '$2,156.78',
            cardNumber: '4512-XXXX-1678-7528',
            cardCompany: 'VISA',
            expiry: 'Expires at 02/20'
        },
        {
            name: 'Mrs. Angelina Jolie',
            balance: '$1,467.98',
            cardNumber: '4512-XXXX-1678-7528',
            cardCompany: 'VISA',
            expiry: 'Locked Temporary'
        }
    ],
    teams: [
        {
            name: 'Team 1',
            members: [ 'NC','JBK','KRD'],
            currentMonthCharged: '$10,200.00'
        },
        {
            name: 'Team 2',
            members: [ 'CML','NC','JBK','KRD'],
            currentMonthCharged: '$10,500.00'
        },
        {
            name: 'Team 3',
            members: [ 'CML','KRD'],
            currentMonthCharged: '$12,200.00'
        },
        {
            name: 'Team 4',
            members: ['CML','NC','JBK','KRD'],
            currentMonthCharged: '$10,200.00'
        }
    ]
}

export const CatData = [
    {
        id: 1,
        productName: 'ELLIOT GLASSES',
        currentPrice: '$678.00',
        discountedPrice: '$754.00',
        new: true,
        previewImage: product1
    },
    {
        id: 2,
        productName: 'MAXTOR CHAIR',
        currentPrice: '$166.00',
        discountedPrice: '$245.00',
        new: true,
        previewImage: product2
    },
    {
        id: 3,
        productName: 'MANROAD GLASSES',
        currentPrice: '$199.00',
        discountedPrice: '',
        new: false,
        previewImage: product3
    },
    {
        id: 4,
        productName: 'ALIGN GLASSES',
        currentPrice: '$356.00',
        discountedPrice: '$542.00',
        new: true,
        previewImage: product4
    },
    {
        id: 5,
        productName: 'ELLIOT GLASSES',
        currentPrice: '$678.00',
        discountedPrice: '$754.00',
        new: false,
        previewImage: product1
    },
    {
        id: 6,
        productName: 'MAXTOR CHAIR',
        currentPrice: '$166.00',
        discountedPrice: '$245.00',
        new: true,
        previewImage: product2
    }
]