import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { userDataType } from "../pages/loginpage/Types";

interface stateType{
    cart: string[],
    navbarVisible: boolean,
    loggedIn: boolean,
    userInfo: userDataType | null
}

const initialState: stateType = {
    cart: [],
    navbarVisible: true,
    loggedIn: false,
    userInfo: null
}

export const primarySlice  = createSlice({
    name: 'PrimarySlice',
    initialState,
    reducers: { 
        toggleNav: (state) => {
            state.navbarVisible = !state.navbarVisible
        },
        addToCart: (state, action: PayloadAction<string>) => {
            state.cart.push(action.payload)
        },
        addLoggedInUser: (state, action: PayloadAction<userDataType>) => {
            state.loggedIn = true
            state.userInfo = action.payload
        },
        logoutUser: (state) => {
            state.loggedIn = false
            state.userInfo = null
        }
    }
})

export const { toggleNav, addToCart, addLoggedInUser, logoutUser } = primarySlice.actions

export default primarySlice.reducer 