import { configureStore } from "@reduxjs/toolkit";
import primaryReducer from "../redux/slice"

export const myStore = configureStore({
    reducer: {
        primary: primaryReducer
    }
})

export type RootState = ReturnType<typeof myStore.getState>
export type AppDispatch = typeof myStore.dispatch