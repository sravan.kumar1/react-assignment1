import React from 'react';
import './App.scss';
import LoginHome from './pages/loginpage/LoginHome';
import {Routes, Route} from 'react-router-dom'
import Home from './pages/home/Home';
import CatalogMain from './pages/catalogs/CatalogMain';
import ProductDetails from './pages/catalogs/productdetails/ProductDetails';

function App() {

  return (
    <div className="App">
      <Routes>
        <Route index element={<LoginHome/>}/>
        <Route path='/home' element={<Home />}/>
        <Route path='/catalog' element={<CatalogMain />}>
          <Route path=':productId' element={<ProductDetails/>}/>
        </Route>
        <Route path='/*' element={<h2 style={{textAlign: 'center'}}>Wrong URL</h2>}/>
      </Routes>
    </div>
  );
}

export default App;
